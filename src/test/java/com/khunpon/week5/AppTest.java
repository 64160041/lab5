package com.khunpon.week5;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAdd1And1Is2(){
        int result = App.add(1,1);
        assertEquals(2, result);
    }
    @Test
    public void shouldAdd2And1Is3(){
        int result = App.add(2,1);
        assertEquals(3, result);
    }
    @Test
    public void shouldAddMin1And0IsMin1(){
        int result = App.add(-1,0);
        assertEquals(-1, result);
    }
}
